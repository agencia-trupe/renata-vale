<?php return array (
  'bio-controller' => 'App\\Http\\Livewire\\BioController',
  'contato-controller' => 'App\\Http\\Livewire\\ContatoController',
  'home' => 'App\\Http\\Livewire\\Home',
  'series-controller' => 'App\\Http\\Livewire\\SeriesController',
  'show-controller' => 'App\\Http\\Livewire\\ShowController',
  'template-controller' => 'App\\Http\\Livewire\\TemplateController',
);
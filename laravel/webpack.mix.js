const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel applications. By default, we are compiling the CSS
 | file for the application as well as bundling up all the JS files.
 |
 */

 mix.setPublicPath("../public")
 .options({
     terser: { extractComments: false },
     processCssUrls: false,
 })
 .js("resources/assets/js/main.js", "assets/js")
 .js("resources/assets/painel/painel.js", "assets/js")
 .stylus("resources/assets/stylus/main.styl", "assets/css", {
     stylusOptions: {
         use: [require("rupture")()],
     },
 })
 .stylus("resources/assets/painel/painel.styl", "assets/css", {
     stylusOptions: {
         use: [require("rupture")()],
     },
 })
 .browserSync("localhost:8000");

mix.inProduction() && mix.version();
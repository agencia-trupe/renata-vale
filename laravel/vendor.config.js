var vendor = {
    main: {
        js: [
            // "jquery.cycle2/src/jquery.cycle2.min.js"
        ],

        css: [],
    },

    painel: {
        js: [
            "bootstrap/dist/js/bootstrap.min.js",
            "jquery-ui-dist/jquery-ui.min.js",
            "bootbox/bootbox.js",
            "blueimp-file-upload/js/jquery.fileupload.js",
            "datatables/media/js/jquery.dataTables.min.js",
            "clipboard/dist/clipboard.min.js",
        ],

        css: [
            "datatables/media/css/jquery.dataTables.min.css",
            "jquery-ui-dist/jquery-ui.min.css",
        ],
    },
};

var jsPath = "../public/assets/js/";
var cssPath = "../public/assets/css/";

var fs = require("fs");

function concat(opts) {
    var fileList = opts.src;
    var distPath = opts.dist;
    var out = fileList.map(function (filePath) {
        return fs.readFileSync("../public/assets/vendor/" + filePath, "utf-8");
    });

    fs.writeFileSync(distPath, out.join("\n"), "utf-8");
    console.log(distPath + " built.");
}

if (!fs.existsSync(jsPath)) {
    fs.mkdirSync(jsPath);
}
if (!fs.existsSync(cssPath)) {
    fs.mkdirSync(cssPath);
}

concat({
    src: vendor.main.js,
    dist: jsPath + "vendor.main.js",
});

concat({
    src: vendor.painel.js,
    dist: jsPath + "vendor.painel.js",
});

concat({
    src: vendor.main.css,
    dist: cssPath + "vendor.main.css",
});

concat({
    src: vendor.painel.css,
    dist: cssPath + "vendor.painel.css",
});

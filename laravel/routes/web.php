<?php

use Illuminate\Support\Facades\Route;

use App\Http\Livewire\Home;
use App\Http\Livewire\BioController;
use App\Http\Livewire\ContatoController;
use App\Http\Livewire\ShowController;
use App\Http\Controllers\ProjetosController;
use App\Http\Livewire\PoliticaDePrivacidadeController;



Route::get('/', Home::class)->name('home');
Route::get('/bio', BioController::class)->name('bio');
Route::get('contato', ContatoController::class)->name('contato');

Route::post('contato', [ContatoController::class, 'post'])->name('contato.post');

Route::get('series', [ProjetosController::class, 'index'])->name('series');
Route::get('series/{categoria_slug}', [ProjetosController::class, 'chamada'])->name('chamada');
Route::get('series/{categoria_slug}/{projeto_slug}', ShowController::class)->name('livewire.show');

Route::get('politica-de-privacidade', PoliticaDePrivacidadeController::class)->name('politica');
Route::post('aceite-de-cookies', [Home::class, 'postCookies'])->name('aceite-de-cookies.post');



require __DIR__.'/auth.php';

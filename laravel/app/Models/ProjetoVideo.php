<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProjetoVideo extends Model
{
    protected $table = 'projetos_videos';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }

    public function scopeProjeto($query, $id)
    {
        return $query->where('projeto_id', $id);
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use App\Helpers\CropImage;

class Contato extends Model
{
    use HasFactory;
    use Notifiable;

    protected $table = 'contatos';

    protected $guarded = ['id'];

    public static function upload_imagem()
    {
        return CropImage::make('imagem', [
            'width'  => 950,
            'height' => 1200,
            'path'   => 'assets/img/contato/'
        ]);
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use App\Helpers\CropImage;

class ProjetoImagem extends Model
{
    use HasFactory;
    
    protected $table = 'projetos_imagens';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }

    public function scopeProjeto($query, $id)
    {
        return $query->where('projeto_id', $id);
    }

    public static function uploadImagem()
    {
        return CropImage::make('imagem', [
            [
                'width'   => null,
                'height'  => 500,
                'path'    => 'assets/img/projetos/imagens/'
            ],
            [
                'width'   => null,
                'height'  => 1080,
                'path'    => 'assets/img/projetos/imagens/highres/'
            ],
        ]);
    }
}

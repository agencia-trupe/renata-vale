<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;
use Cviebrock\EloquentSluggable\Sluggable;

class Projeto extends Model 
{
    use Sluggable;

    public function sluggable(): array
    {
        return [
            'slug' => [
                'source' => 'titulo',
                'on_update' => true
            ]   
        ];           
    }
    
    protected $table = 'projetos';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }

    public function scopeCategoria($query, $categoria_id)
    {
        return $query->where('projetos_categoria_id', $categoria_id);
    }

    public function categoria()
    {
        return $this->belongsTo('App\Models\ProjetoCategoria', 'projetos_categoria_id');
    }

    public function imagens()
    {
        return $this->hasMany('App\Models\ProjetoImagem', 'projeto_id')->ordenados();
    }

    public function videos()
    {
        return $this->hasMany('App\Models\ProjetoVideo', 'projeto_id')->ordenados();
    }

    public static function upload_capa()
    {
        return CropImage::make('capa', [
            [
                'width'  => 720,
                'height' => 370,
                'path'   => 'assets/img/projetos/'
            ],
            [
                'width'   => 380,
                'height'  => 240,
                'path'    => 'assets/img/projetos/thumbs/'
            ],
        ]);
    }
}

<?php

namespace App\Providers;

use App\Models\AceiteDeCookies;
use App\Models\Configuracao;
use App\Models\Contato;
use App\Models\Servico;
use App\Models\ContatoRecebido;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        View::composer('*', function ($view) {
            $view->with('config', Configuracao::first());
        });

        View::composer('site.*', function ($view) {
            $view->with('contato', Contato::first());

            $request = app(\Illuminate\Http\Request::class);
            $view->with('verificacao', AceiteDeCookies::where('ip', $request->ip())->first());
        });

        View::composer('livewire.*', function ($view) {
            $view->with('contato', Contato::first());

            $request = app(\Illuminate\Http\Request::class);
            $view->with('verificacao', AceiteDeCookies::where('ip', $request->ip())->first());
        });

        View::composer('painel.layout.*', function ($view) {
            $view->with('contatosNaoLidos', ContatoRecebido::naoLidos()->count());
        });
    }
}

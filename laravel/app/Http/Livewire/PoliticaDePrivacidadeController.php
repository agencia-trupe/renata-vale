<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\PoliticaDePrivacidade;


class PoliticaDePrivacidadeController extends Component
{
    public function render()
    {
        $politica = PoliticaDePrivacidade::first();

        return view('livewire.politica-de-privacidade', compact('politica'))->layout('layouts.template');
    }

}

<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\ProjetoCategoria;
use App\Models\Projeto;
use App\Models\ProjetoImagem;

class ShowController extends Component
{
    public $categoriaSlug;

    public $projetoSlug;

    public function mount($categoria_slug, $projeto_slug)
    {

        $this->categoriaSlug = $categoria_slug;

        $this->projetoSlug = $projeto_slug;

    }
    
    public function render()
    {

        $projeto_slug = $this->projetoSlug;
        $categoria_slug = $this->categoriaSlug;

        //Puxando do banco a informação que está no endereço do browser (projeto)
        $composicao = Projeto::where('slug', $projeto_slug)->first();

        
        //Puxando do banco a informação que está no endereço do browser (Categoria)
        $serieslug = ProjetoCategoria::where('slug', 'LIKE', "%$categoria_slug%")->first();
        
        //Separando Id para criar seletor
        $catId = $serieslug->id;
        
        //Puxando do banco e comparando os dados com um filtro para puxar projetos por série
        $projeto = Projeto::ordenados()->where('projetos_categoria_id', 'LIKE', "$catId")->get();
        
        $composicao2 = Projeto::orderBy('ordem', 'ASC')->where('projetos_categoria_id',  $catId)->where('slug', $projeto_slug)->first();
        

        try{
            $next = Projeto::ordenados()->where('ordem', ">", $composicao2->ordem)->where('projetos_categoria_id', "$catId")->first();
            
            if(isset($next)){
                //teste 
            }  
            else{
                $next = 'nulo';
            }

        }
        catch (\Exception $e){
            
            return back();
        }

        if (empty($next)) {
            $next = $composicao;
        }
        
        
        //Puxando imagens apenas daquela composição
        $imagens = ProjetoImagem::projeto($composicao->id)->ordenados()->get();

        $series = ProjetoCategoria::ordenados()->get();

        return view('livewire.show', compact('composicao', 'imagens', 'serieslug','series','projeto', 'next'))->layout('layouts.template');
    }
}

<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\Bio;

class BioController extends Component
{
    public function render()
    {
        $bio = Bio::first();

        return view('livewire.bio', compact('bio'))->layout('layouts.template');
    }
}

<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\Banner;
use Illuminate\Http\Request;
use App\Models\AceiteDeCookies;

class Home extends Component
{
    public function render()
    {
        $banners = Banner::ordenados()->get();

        return view('livewire.home', compact('banners'))->layout('layouts.template');
    }

    public function postCookies(Request $request)
    {
        $input = $request->all();
        $input['ip'] = $request->ip();

        AceiteDeCookies::create($input);

        return redirect('/');
    }
}

<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Http\Requests\ContatosRecebidosRequest;
use App\Models\Contato;
use App\Models\ContatoRecebido;
use App\Notifications\ContatosRecebidosNotification;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Notification;

class ContatoController extends Component
{
    public function render()
    {
        // $contato = Contato::first();

        return view('livewire.contato')->layout('layouts.template');
    }

    public function post(ContatosRecebidosRequest $request, ContatoRecebido $contato_recebido)
    {
        try {
            $data = $request->all();

            $contato_recebido->create($data);

            Notification::send(
                Contato::first(),
                new ContatosRecebidosNotification($data)
            );

            return back()->with('enviado', true);
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao enviar contato: ' . $e->getMessage()]);
        }
    }
}

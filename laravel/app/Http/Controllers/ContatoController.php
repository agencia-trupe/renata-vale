<?php

namespace App\Http\Controllers;

use App\Http\Requests\ContatosRecebidosRequest;
use App\Models\Contato;
use App\Models\ContatoRecebido;
use App\Notifications\ContatosRecebidosNotification;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Notification;

class ContatoController extends Controller
{
    public function index()
    {
        return view('site.contato');
    }

    public function getDados()
    {
        $contato = Contato::first();

        return response()->json(['contato' => $contato]);
    }

    public function post(ContatosRecebidosRequest $request, ContatoRecebido $contato_recebido)
    {
        try {
            $data = $request->all();

            $contato_recebido->create($data);

            Notification::send(
                Contato::first(),
                new ContatosRecebidosNotification($data)
            );

            return back()->with('enviado', true);
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao enviar contato: ' . $e->getMessage()]);
        }
    }
}

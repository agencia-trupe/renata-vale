<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\ProjetosRequest;
use App\Http\Controllers\Controller;

use App\Models\Projeto;
use App\Models\ProjetoCategoria;
use App\Models\ProjetoImagem;

class ProjetosController extends Controller
{
    // private $categorias;

    public function __construct()
    {
        $this->categorias = ProjetoCategoria::ordenados()->get('titulo', 'id');
    }

    // public function index()
    // {

    //     $categorias = ProjetoCategoria::ordenados()->get();

    //     if (isset($_GET['categoria'])) {
    //         $categoria = $_GET['categoria'];
    //         $registros = Projeto::where('projetos_categoria_id', $categoria)
    //             ->join('projetos_categorias', 'projetos_categorias.id', '=', 'projetos.projetos_categoria_id')
    //             ->select('projetos_categorias.titulo as categoria', 'projetos.*')
    //             ->ordenados()->get();
    //     } else {
    //         $registros = Projeto::join('projetos_categorias', 'projetos_categorias.id', '=', 'projetos.projetos_categoria_id')
    //             ->select('projetos_categorias.titulo as categoria', 'projetos.*')
    //             ->ordenados()->get();
    //     }
        
    //     $categoria_fechada = ProjetoCategoria::where('id', '0')->ordenados()->get();

    //     return view('painel.projetos.index', compact('categorias', 'registros', 'categoria_fechada'));
    // }

    public function index()
    {

        $categorias = ProjetoCategoria::ordenados()->get();

        $categoria1 = ProjetoCategoria::ordenados()->first();

        $registros = Projeto::where('projetos_categoria_id', $categoria1->id)->ordenados()->get();

        return view('painel.projetos.index', compact('categorias', 'registros'));
    }

    public function group($group)
    {

        $categorias = ProjetoCategoria::ordenados()->get();

        $cat_chosen = ProjetoCategoria::where('slug', $group)->first();

        $registros = Projeto::where('projetos_categoria_id', $cat_chosen->id)->ordenados()->get();

        return view('painel.projetos.group', compact('categorias', 'group', 'registros'));
    }

    public function create()
    {
        $categorias = ProjetoCategoria::ordenados()->pluck('titulo','id');

        

        return view('painel.projetos.create', compact('categorias'));
    }

    public function store(ProjetosRequest $request)
    {

        try {

            $input = $request->all();

            if (isset($input['capa'])) $input['capa'] = Projeto::upload_capa();

            Projeto::create($input);
           
            return redirect()->route('projetos.index')->with('success', 'Registro adicionado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: ' . $e->getMessage()]);
        }
    }

    public function edit(Projeto $projeto)
    {

        $categorias = ProjetoCategoria::ordenados()->pluck('titulo','id');

        return view('painel.projetos.edit', compact('projeto', 'categorias'));

    }

    public function update(ProjetosRequest $request, Projeto $projeto)
    {
       
        try {

            $input = $request->all();

            if (isset($input['capa'])) $input['capa'] = Projeto::upload_capa();

            $projeto->update($input);

            return redirect()->route('projetos.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: ' . $e->getMessage()]);
        }
    }

    public function destroy(Projeto $projeto)
    {
        try {

            $projeto->delete();

            return redirect()->route('projetos.index')->with('success', 'Registro excluído com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: ' . $e->getMessage()]);
        }
    }
}

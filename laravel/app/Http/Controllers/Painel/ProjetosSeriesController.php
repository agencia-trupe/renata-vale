<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\ProjetosSeriesRequest;
use App\Http\Controllers\Controller;

use App\Models\ProjetoCategoria;

class ProjetosseriesController extends Controller
{
    public function index()
    {
        
        $series = ProjetoCategoria::ordenados()->get();

        
        return view('painel.projetos.series.index', compact('series'));
    }

    public function create()
    {
        return view('painel.projetos.series.create');
        
    }

    public function store(ProjetosseriesRequest $request)
    {
        
        try {

            $input = $request->all();

            ProjetoCategoria::create($input);
            return redirect()->route('series.index')->with('success', 'serie adicionada com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar serie: '.$e->getMessage()]);

        }
    }

    public function edit(ProjetoCategoria $series)
    {
        return view('painel.projetos.series.edit', compact('series'));
    }

    public function update(ProjetosseriesRequest $request, ProjetoCategoria $series)
    {
        try {

            $input = $request->all();

            $series->update($input);
            return redirect()->route('series.index')->with('success', 'serie alterada com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar serie: '.$e->getMessage()]);

        }
    }

    public function destroy(ProjetoCategoria $series)
    {
        try {

            $series->delete();
            return redirect()->route('series.index')->with('success', 'serie excluída com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir serie: '.$e->getMessage()]);

        }
    }
}

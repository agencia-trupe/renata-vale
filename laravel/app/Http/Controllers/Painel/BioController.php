<?php

namespace App\Http\Controllers\Painel;

use App\Http\Controllers\Controller;
use App\Http\Requests\BioRequest;
use App\Models\Bio;
use Illuminate\Http\Request;

class BioController extends Controller
{
    public function index()
    {
        $bio = Bio::first();

        return view('painel.bio.edit', compact('bio'));
    }

    public function update(BioRequest $request, Bio $bio)
    {
        try {
            $input = $request->all();

            if (isset($input['imagem'])) $input['imagem'] = Bio::upload_imagem();

            $bio->update($input);

            return redirect()->route('bio.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: ' . $e->getMessage()]);
        }
    }
}

<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\ProjetosImagensRequest;
use App\Http\Controllers\Controller;

use App\Models\Projeto;
use App\Models\ProjetoImagem;

use App\Helpers\CropImage;

class ProjetosImagensController extends Controller
{
    public function index(Projeto $projeto)
    {

        $registros = ProjetoImagem::projeto($projeto->id)->ordenados()->get();

        return view('painel.projetos.imagens.index', compact('projeto', 'registros'));
    }


    public function create(Projeto $projeto)
    {

        return view('painel.projetos.imagens.create', compact('projeto'));

    }

    public function store(ProjetosImagensRequest $request, Projeto $projeto, ProjetoImagem $imagen)
    {
        try {
            $input = $request->all();

            if (isset($input['imagem'])) $input['imagem'] = ProjetoImagem::uploadImagem();

            ProjetoImagem::create($input);

            return redirect()->route('projetos.imagens.index', ['projeto' => $projeto, 'imagen' => $imagen])->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {
            return 'Erro ao adicionar imagem: ' . $e->getMessage();
        }
    }

    public function edit(Projeto $projeto, ProjetoImagem $imagen)
    {

        return view('painel.projetos.imagens.edit', compact('projeto', 'imagen'));

    }

    public function update(ProjetosImagensRequest $request, Projeto $projeto, ProjetoImagem $imagen)
    {
       
        try {

            $input = $request->all();

            if (isset($input['imagem'])) $input['imagem'] = ProjetoImagem::uploadImagem();

            $imagen->update($input);

            return redirect()->route('projetos.imagens.index', ['projeto' => $projeto, 'imagen' => $imagen])->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: ' . $e->getMessage()]);
        }
    }

    public function destroy(Projeto $projeto, ProjetoImagem $imagen)
    {
        try {
            
            $imagen->delete();
            return redirect()->route('projetos.imagens.index', $projeto->id)
                ->with('success', 'Imagem excluída com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir imagem: ' . $e->getMessage()]);
        }
    }

}

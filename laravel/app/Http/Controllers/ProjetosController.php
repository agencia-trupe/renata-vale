<?php

namespace App\Http\Controllers;

use App\Models\ProjetoCategoria;
use App\Models\Projeto;
use App\Models\ProjetoImagem;
use Illuminate\Routing\Route;

class ProjetosController extends Controller
{
    public function __construct()
    {
        view()->share('series', ProjetoCategoria::ordenados()->get());
    }


    public function index()
    {

        $series = ProjetoCategoria::ordenados()->get();

        $projeto = $series . Projeto::ordenados()->firstOrFail();
  
        
        return view('site.series', compact('series','projeto'));
    }


    public function chamada($categoria_slug)
    {
        try{

            $serieslug = ProjetoCategoria::where('slug', 'LIKE', "%$categoria_slug%")->first();
        
            $catId = $serieslug->id;
    
            $projeto = Projeto::ordenados()->where('projetos_categoria_id', 'LIKE', "$catId")->first();
    
    
            return redirect()->route('livewire.show', ['categoria_slug' => $serieslug->slug, 'projeto_slug' => $projeto->slug]);    

        }
        catch (\Exception $e){
            return back();
        }
        
    }

    public function show($categoria_slug, $projeto_slug)
    {
        //Puxando do banco a informação que está no endereço do browser (projeto)
        $composicao = Projeto::where('slug', $projeto_slug)->first();

        
        //Puxando do banco a informação que está no endereço do browser (Categoria)
        $serieslug = ProjetoCategoria::where('slug', 'LIKE', "%$categoria_slug%")->first();
        
        //Separando Id para criar seletor
        $catId = $serieslug->id;
        
        //Puxando do banco e comparando os dados com um filtro para puxar projetos por série
        $projeto = Projeto::ordenados()->where('projetos_categoria_id', 'LIKE', "$catId")->get();
        
        dd($catId);

        $composicao2 = Projeto::where('slug', $projeto_slug)->where('projetos_categoria_id', 'LIKE', "$catId")->first();
        

        try{
            $next = Projeto::ordenados()->where('ordem', ">", $composicao2->ordem)->where('projetos_categoria_id', "$catId")->first();    
        }
        catch (\Exception $e){
            return back();
        }

        if (empty($next)) {
            $next = $composicao;
        }
        
        
        //Puxando imagens apenas daquela composição
        $imagens = ProjetoImagem::projeto($composicao->id)->ordenados()->get();

        $series = ProjetoCategoria::ordenados()->get();
        
        return view('site.show', compact('composicao', 'imagens', 'serieslug','series','projeto', 'next'));
    }


    public function getShowDadosProximo($categoria_slug, $projeto_slug)
    {
        //Puxando do banco a informação que está no endereço do browser (projeto)
        $composicao = Projeto::where('slug', $projeto_slug)->first();

        
        //Puxando do banco a informação que está no endereço do browser (Categoria)
        $serieslug = ProjetoCategoria::where('slug', 'LIKE', "%$categoria_slug%")->first();
        
        //Separando Id para criar seletor
        $catId = $serieslug->id;
        
        //Puxando do banco e comparando os dados com um filtro para puxar projetos por série
        $projeto = Projeto::ordenados()->where('projetos_categoria_id', 'LIKE', "$catId")->get();
        
        $composicao2 = Projeto::where('slug', $projeto_slug)->where('projetos_categoria_id', 'LIKE', "$catId")->first();
        

        try{
            $next = Projeto::ordenados()->where('ordem', ">", $composicao2->ordem)->where('projetos_categoria_id', "$catId")->first();    
        }
        catch (\Exception $e){
            return back();
        }

        if (empty($next)) {
            $next = $composicao;
        }
        
        
        //Puxando imagens apenas daquela composição
        $imagens = ProjetoImagem::projeto($composicao->id)->ordenados()->get();

        $series = ProjetoCategoria::ordenados()->get();

        return response()->json(['composicao' => $composicao, 'imagens' => $imagens, 'serieslug' => $serieslug, 'projeto' => $projeto, 'series' => $series, 'next' => $next]);
        
    }

}

<?php

namespace App\Http\Controllers;

use App\Models\Bio;
use Illuminate\Http\Request;

class BioController extends Controller
{
    public function biofront()
    {
        $bio = Bio::first();

        return view('site.bio', compact('bio'));
    }


}

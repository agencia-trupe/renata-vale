<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ContatosRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email'           => 'required|email',
            'telefone'        => 'required',
            'celular'         => 'required',
            'instagram'       => 'required',
            'endereco'     => 'required',
            'bairro'       => 'required',
            'cidade'       => 'required',
            'pais'         => 'required',
            'cep'             => 'required',
            'google_maps'     => 'required',
        ];
    }
}

<?php

namespace Database\Seeders;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Seeder;

class ContatosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('contatos')->insert([
            'email' => 'contato@trupe.net',
            'telefone' => '11 3044 6812',
            'instagram' => 'https://instagram.com',
            'endereco' => 'R. Henrique Martins, 751 &middot; Jardim Paulista<br>01435-010 &middot; São Paulo, SP<br>Brasil',
        ]);
    }
}

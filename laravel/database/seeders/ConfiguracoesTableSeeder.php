<?php

namespace Database\Seeders;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Seeder;

class ConfiguracoesTableSeeder extends Seeder

{
    public function run()
    {
        DB::table('configuracoes')->insert([
            'title' => 'Cenografia',
        ]);
    }
}

<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class CreateProjetosTable extends Migration
{
    public function up()
    {
        Schema::create('projetos_categorias', function (Blueprint $table) {
            $table->id();
            $table->integer('ordem')->default(0);
            $table->string('titulo');
            $table->string('slug');
            $table->timestamps();
        });

        Schema::create('projetos', function (Blueprint $table) {
            $table->id();
            $table->integer('projetos_categoria_id')->unsigned()->nullable();
            $table->integer('ordem')->default(0);
            $table->string('slug');
            $table->string('titulo');
            $table->string('ano')->nullable();
            $table->text('descricao');
            $table->foreign('projetos_categoria_id')->references('id')->on('projetos_categorias')->onDelete('set null');
            $table->timestamps();
        });

        Schema::create('projetos_imagens', function (Blueprint $table) {
            $table->id();
            $table->foreignId('projeto_id')->constrained('projetos')->onDelete('cascade');
            $table->integer('ordem')->default(0);
            $table->string('imagem');
            $table->string('description');
            $table->timestamps();
        });

        Schema::create('projetos_videos', function (Blueprint $table) {
            $table->increments('id');
            $table->foreignId('projeto_id')->constrained('projetos')->onDelete('cascade');
            $table->integer('ordem')->default(0);
            $table->string('titulo');
            $table->string('video');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('projetos_videos');
        Schema::drop('projetos_imagens');
        Schema::drop('projetos');
        Schema::drop('projetos_categorias');
    }
}

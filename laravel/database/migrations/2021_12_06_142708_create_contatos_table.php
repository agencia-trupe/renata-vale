<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateContatosTable extends Migration
{
    public function up()
    {
        Schema::create('contatos', function (Blueprint $table) {
            $table->id();
            $table->string('imagem');
            $table->string('email');
            $table->string('telefone');
            $table->string('celular');
            $table->string('instagram');
            $table->string('endereco');
            $table->string('bairro');
            $table->string('cidade');
            $table->string('pais');
            $table->string('cep');
            $table->text('google_maps');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('contatos');
    }
}

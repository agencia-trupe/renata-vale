<header>

    <a href="{{ route('home') }}"><img src="{{ asset('assets/img/layout/marca-renatavale.svg') }}" class="logo1" alt=""></a>

    <nav id="nav-desktop">

            <a href="{{ route('series') }}" class="link-nav">
                séries
            </a>
           
            <a href="{{ route('bio') }}" class="link-nav">
                <div>
                bio
                </div>
            </a>

            <a href="{{ route('contato') }}" class="link-nav">
                <div>
                contato
                </div>
            </a>

            @if($contato->instagram)
                <a href="{{$contato->instagram}}" class="instagram" >
                    <img src="{{ asset('assets/img/layout/icone-instagram.svg') }}" alt="">
                </a>
            @endif
           
        </nav>

        <button id="mobile-toggle" type="button" role="button">
            <span class="lines"></span>
        </button>

    </div>

    <nav id="nav-mobile">
        <div class="center-header">
        
            <a href="{{ route('home') }}">
                home
            </a>
            <a href="{{ route('series') }}">
                séries
            </a>
            <a href="{{ route('bio') }}">
                bio
            </a>
            <a href="{{ route('contato') }}">
                contato
            </a>   

            @if($contato->instagram)
                <a href="{{$contato->instagram}}" class="link-nav"><img src="{{ asset('assets/img/layout/icone-instagram.svg') }}" alt=""></a>
            @endif
        </div>
    </nav>

</header>
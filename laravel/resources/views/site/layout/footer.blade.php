<footer>
    <div class="rights">
        <a href="{{ route('politica') }}"><p>[POLÍTICA DE PRIVACIDADE] </p></a>
        <div class="part2">
            <p class="dados" style="text-transform: uppercase;"> © {{ date('Y') }} {{ config('app.name') }}</p>
            <p class="direitos"> • Todos os direitos reservados |</p>
            <a href="https://www.trupe.net" target="_blank" class="link-trupe"><p> Criação de sites: </p></a>
            <a href="https://www.trupe.net" target="_blank" class="link-trupe"><p>Trupe Agência Criativa</p></a>
        </div>
    </div>
</footer>
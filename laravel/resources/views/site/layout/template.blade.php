<!doctype html>
<html lang="pt-BR">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="robots" content="index, follow">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <base href="{{ url('/') }}">

    <meta name="author" content="Trupe Agência Criativa">
    <meta name="copyright" content="{{ date('Y') }} Trupe Agência Criativa">
    <meta name="description" content="{{ $config->description }}">
    <meta name="keywords" content="{{ $config->keywords }}">

    <meta property="og:title" content="{{ $config->title }}">
    <meta property="og:description" content="{{ $config->description }}">
    <meta property="og:site_name" content="{{ $config->{trans('database.title')} }}">
    <meta property="og:type" content="website">
    <meta property="og:url" content="{{ Request::url() }}">
    @if($config->imagem_de_compartilhamento)
    <meta property="og:image" content="{{ asset('assets/img/'.$config->imagem_de_compartilhamento) }}">
    @endif

    <title>{{ $config->title }}</title>

    <link rel="stylesheet" href="{{ asset('assets/vendor/fancybox/source/jquery.fancybox.css') }}">

    <script src="{{ asset('assets/js/hoverdir/js/modernizr.custom.97074.js') }}"></script>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/js/hoverdir/css/style.css') }}" />
    <link rel="stylesheet" href="{{ asset('assets/css/main.css') }}">

    <noscript><link rel="stylesheet" type="text/css" href="{{ asset('assets/js/hoverdir/css/noJS.css') }}"/></noscript>

    <!-- FAVICON -->
    <link rel="apple-touch-icon" sizes="57x57" href="{{ asset('assets/img/favicon/apple-icon-57x57.png') }}">
    <link rel="apple-touch-icon" sizes="60x60" href="{{ asset('assets/img/favicon/apple-icon-60x60.png') }}">
    <link rel="apple-touch-icon" sizes="72x72" href="{{ asset('assets/img/favicon/apple-icon-72x72.png') }}">
    <link rel="apple-touch-icon" sizes="76x76" href="{{ asset('assets/img/favicon/apple-icon-76x76.png') }}">
    <link rel="apple-touch-icon" sizes="114x114" href="{{ asset('assets/img/favicon/apple-icon-114x114.png') }}">
    <link rel="apple-touch-icon" sizes="120x120" href="{{ asset('assets/img/favicon/apple-icon-120x120.png') }}">
    <link rel="apple-touch-icon" sizes="144x144" href="{{ asset('assets/img/favicon/apple-icon-144x144.png') }}">
    <link rel="apple-touch-icon" sizes="152x152" href="{{ asset('assets/img/favicon/apple-icon-152x152.png') }}">
    <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('assets/img/favicon/apple-icon-180x180.png') }}">
    <link rel="icon" type="image/png" sizes="192x192" href="{{ asset('assets/img/favicon/android-icon-192x192.png') }}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('assets/img/favicon/favicon-32x32.png') }}">
    <link rel="icon" type="image/png" sizes="96x96" href="{{ asset('assets/img/favicon/favicon-96x96.png') }}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('assets/img/favicon/favicon-16x16.png') }}">
    <link rel="manifest" href="{{ asset('assets/img/favicon/manifest.json') }}">

    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="{{ asset('assets/img/favicon/ms-icon-144x144.png') }}">
    <meta name="theme-color" content="#ffffff">

    <!-- Start WOWSlider.com HEAD section -->
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/js/wowslider/engine1/style.css') }}" />
    <script type="text/javascript" src="{{ asset('assets/js/wowslider/engine1/jquery.js') }}"></script>
    <!-- End WOWSlider.com HEAD section -->

    @livewireStyles

    @if($config->codigo_gtm)
    @include('site.layout.components.script-gtm')
    @endif

    @if($config->pixel_facebook)
    @include('site.layout.components.script-facebook')
    @endif
</head>

<body>

    @if($config->codigo_gtm)
    @include('site.layout.components.noscript-gtm')
    @endif

    @include('site.layout.header')

    @yield('content')

    

    @include('site.layout.footer')
    

     <!-- AVISO DE COOKIES -->
     @if(!isset($verificacao))
    <div class="aviso-cookies">
        <p class="frase-cookies">Usamos cookies para personalizar o conteúdo, acompanhar anúncios e oferecer uma experiência de navegação mais segura a você. Ao continuar navegando em nosso site você concorda com o uso dessas informações. Leia nossa <a href="{{ route('home') }}" target="_blank" class="link-politica">Política de Privacidade</a> e saiba mais.</p>
        <button class="aceitar-cookies">ACEITAR E FECHAR</button>
    </div>
    @endif
    

    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>

    <script src="{{ asset('assets/vendor/jquery/dist/jquery.min.js') }}"></script>
    <script src="{{ asset('assets/vendor/jquery.cycle2/src/jquery.cycle2.min.js') }}"></script>
    <script src="{{ asset('assets/vendor/jquery-mask-plugin-master/dist/jquery.mask.min.js') }}"></script>
    <script src="{{ asset('assets/vendor/fancybox/source/jquery.fancybox.pack.js') }}"></script>
    <script src="{{ asset('assets/vendor/responsive-masonry-grid/dist/jquery.masonryGrid.js') }}"></script>
    <script src="https://unpkg.com/masonry-layout@4/dist/masonry.pkgd.js"></script>
    <script src="{{ asset('assets/js/hoverdir/js/modernizr.custom.97074.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/hoverdir/js/jquery.hoverdir.js') }}"></script>
    <script src="{{ asset('assets/js/main.js') }}"></script>
    <script src="https://unpkg.com/swup@latest/dist/swup.min.js"></script>  

    <script>
        var routeHome = "https://renatavale.com.br/";
    </script>

    @if($config->analytics_ua)
    @include('site.layout.components.script-analytics-ua')
    @endif

    @if($config->analytics_g)
    @include('site.layout.components.script-analytics-g')
    @endif

    @livewireScripts
</body>

</html>
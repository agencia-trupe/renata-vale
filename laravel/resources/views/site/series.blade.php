@extends('site.layout.template')

@section('content')

<main class="series">
    <div class="group">
        @foreach($series as $serie)
        <a href="{{ route('chamada', ['categoria_slug' => $serie->slug]) }}">{{$serie->titulo}}</a>
        @endforeach
    </div>
    
</main>



@endsection
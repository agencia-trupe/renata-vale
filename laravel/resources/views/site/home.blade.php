@extends('site.layout.template')

@section('content')

<main class="home">

    <div class="banners">

    <!-- Start WOWSlider.com BODY section -->   
        <div id="wowslider-container1">
            <div class="ws_images"><ul>
                @foreach($banners as $banner)
                    <li>
                        <a href="{{$banner->link}}"><img src="{{ asset('assets/img/banners/'.$banner->imagem) }}" alt="jquery image slider" id="wows1_0"/></a>
                    </li>
                @endforeach
            </ul></div>
            <div class="ws_shadow"></div>
        </div>	
        <script type="text/javascript" src="{{ asset('assets/js/wowslider/engine1/wowslider.js') }}"></script>
        <script type="text/javascript" src="{{ asset('assets/js/wowslider/engine1/script.js') }}"></script>
       
        <!-- End WOWSlider.com BODY section -->
   
    </div>
</main>


@endsection
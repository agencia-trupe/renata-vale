@extends('site.layout.template')

@section('content')

<main id="swup" class="show transition-fade">
    <div class="navigation">
        <h2>{{$serieslug->titulo}}</h2>
        <div class="external">
            @foreach($projeto as $p)
                <a href="{{ route('show.series', ['categoria_slug' => $serieslug->slug, 'projeto_slug' => $p->slug ]) }}"><div @if($composicao->slug == $p->slug) class="square active arrow" @else class="square" @endif>
                    <img src="{{ asset('assets/img/layout/seta-marcacao.svg')}}"  @if($composicao->slug == $p->slug) style="display:block" @else style="display:none" @endif alt="">
                </div></a>                  
            @endforeach
        </div>
    </div>

    @if($composicao->layout == 0)
    <div class="external_flex">
        <div class="mobile_demo">
            @foreach($imagens as $img)
                <img src="{{ asset('assets/img/projetos/imagens/'.$img->imagem)}}" alt="">
            @endforeach
        </div>
        <div class="pshow-imagembox">
            @foreach($imagens as $img)
            <a href="{{ asset('assets/img/projetos/imagens/highres/'.$img->imagem) }}" class="projeto-imagem" rel="projeto">
                <img src="{{ asset('assets/img/projetos/imagens/'.$img->imagem)}}" alt="" class="img-projeto">
            </a>
            @endforeach
        </div>

        <a href="{{ route('show.series', ['categoria_slug' => $serieslug->slug, 'projeto_slug' => $next->slug ]) }}" class="link-next"><div class="seta_baixo"><img src="{{ asset('assets/img/layout/seta-scrolldown.svg')}}" alt=""></div></a>
    
    </div>
    @endif

    @if($composicao->layout == 1)
    <div class="external_flex1">
        <div class="mobile_demo">
            @foreach($imagens as $img)
                <img src="{{ asset('assets/img/projetos/imagens/'.$img->imagem)}}" alt="">
            @endforeach
        </div>
        <div class="pshow-imagembox1">
            @foreach($imagens as $img)
            <a href="{{ asset('assets/img/projetos/imagens/highres/'.$img->imagem) }}" class="projeto-imagem" rel="projeto"><div>
                <img src="{{ asset('assets/img/projetos/imagens/'.$img->imagem)}}" alt="" class="img-projeto">
            </div></a>
            @endforeach
        </div>

        <a href="{{ route('show.series', ['categoria_slug' => $serieslug->slug, 'projeto_slug' => $next->slug ]) }}" class="link-next"><div class="seta_baixo"><img src="{{ asset('assets/img/layout/seta-scrolldown.svg')}}" alt=""></div></a>
    
    </div>
    @endif
    
    @if($composicao->layout == 2)
    <div class="external_flex2">
        <div class="mobile_demo">
            @foreach($imagens as $img)
                @if($img->ordem == '1')
                    <img class="imgs1" src="{{ asset('assets/img/projetos/imagens/'.$img->imagem)}}" alt="">
                @endif
            @endforeach
            <div class="double">
                @foreach($imagens as $img)
                    @if($img->ordem > '1')
                        <img class="imgs" src="{{ asset('assets/img/projetos/imagens/'.$img->imagem)}}" alt="">
                    @endif
                @endforeach
            </div>
        </div>

        <div class="mobile_demo2">
            @foreach($imagens as $img)
                @if($img->ordem == '1')
                    <a href="{{ asset('assets/img/projetos/imagens/highres/'.$img->imagem) }}" class="projeto-imagem" rel="projeto">
                        <img class="imgs1" src="{{ asset('assets/img/projetos/imagens/'.$img->imagem)}}" alt="">
                    </a>
                @endif
            @endforeach
            <div class="double">
                @foreach($imagens as $img)
                    @if($img->ordem > '1')
                    <a href="{{ asset('assets/img/projetos/imagens/highres/'.$img->imagem) }}" class="projeto-imagem" rel="projeto">
                        <img class="imgs" src="{{ asset('assets/img/projetos/imagens/'.$img->imagem)}}" alt="">
                    </a>
                    @endif
                @endforeach
            </div>
        </div>
        <div class="pshow-imagembox">
            @foreach($imagens as $img)
            <a href="{{ asset('assets/img/projetos/imagens/highres/'.$img->imagem) }}" class="projeto-imagem" rel="projeto"><div>
                <img src="{{ asset('assets/img/projetos/imagens/'.$img->imagem)}}" alt="" class="img-projeto">
            </div></a>
            @endforeach
        </div>

        <a href="{{ route('show.series', ['categoria_slug' => $serieslug->slug, 'projeto_slug' => $next->slug ]) }}" class="link-next"><div class="seta_baixo"><img src="{{ asset('assets/img/layout/seta-scrolldown.svg')}}" alt=""></div></a>
    
    </div>
    @endif

    @if($composicao->layout == 3)
    <div class="external_flex3">
        <div class="mobile_demo">
            @foreach($imagens as $img)
                <img src="{{ asset('assets/img/projetos/imagens/'.$img->imagem)}}" alt="">
            @endforeach
        </div>
        <div class="pshow-imagembox">
            @foreach($imagens as $img)
            <a href="{{ asset('assets/img/projetos/imagens/highres/'.$img->imagem) }}" class="projeto-imagem" rel="projeto"><div>
                <img src="{{ asset('assets/img/projetos/imagens/'.$img->imagem)}}" alt="" class="img-projeto">
            </div></a>
            @endforeach
        </div>

        <a href="{{ route('show.series', ['categoria_slug' => $serieslug->slug, 'projeto_slug' => $next->slug ]) }}" class="link-next"><div class="seta_baixo"><img src="{{ asset('assets/img/layout/seta-scrolldown.svg')}}" alt=""></div></a>
    
    </div>
    @endif
</main>



@endsection
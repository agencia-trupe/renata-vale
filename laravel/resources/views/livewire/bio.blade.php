<main id="swup" class="bio transition-fade">
    <div class="card1">
        <div class="texto">
            <p>{!!$bio->texto_1!!}</p>
        </div>

        <div class="texto2">
            <p>{!!$bio->texto_2!!}</p>
        </div>
    </div>

    <div class="card_image">
        <img src="{{ asset('assets/img/bio/'.$bio->imagem)}}">
    </div>
</main>

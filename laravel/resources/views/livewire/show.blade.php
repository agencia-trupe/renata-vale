<main id="swup" class="show transition-fade">

    <div class="navigation">
        <h2>{{$serieslug->titulo}}</h2>
        <div class="external">
            @foreach($projeto as $p)
            <a href="{{ route('livewire.show', ['categoria_slug' => $serieslug->slug, 'projeto_slug' => $p->slug ]) }}"><div @if($composicao->slug == $p->slug) class="square active arrow" @else class="square" @endif>
                <img src="{{ asset('assets/img/layout/seta-marcacao.svg')}}"  @if($composicao->slug == $p->slug) style="display:block" @else style="display:none" @endif alt="">
            </div></a>                  
            @endforeach
        </div>
    </div>

    <div>

        @if($composicao->layout == 0)
        <div class="external_flex" id="external_flex">
            @if(count($imagens) > 1)
            <div class="mobile_demo">
                @foreach($imagens as $img)
                    <img src="{{ asset('assets/img/projetos/imagens/'.$img->imagem)}}" alt="">
                @endforeach
            </div>
            @endif
            <div class="pshow-imagembox">
                @foreach($imagens as $img)
                <a href="{{ asset('assets/img/projetos/imagens/highres/'.$img->imagem) }}" data-lightbox="image-1" data-title="{{ str_replace(array("<p>", "</p>", "<strong>", "</strong>", "<bold>", "</bold>",),'', $img->description) }}" rel="projeto">
                    <img src="{{ asset('assets/img/projetos/imagens/'.$img->imagem)}}" alt="" class="img-projeto">
                </a>
                @endforeach
            </div>

            @if($next == 'nulo')
                <a style="display:none" href="" class="link-next"><div class="seta_baixo"><img src="{{ asset('assets/img/layout/seta-scrolldown.svg')}}" alt=""></div></a>
            @else
                <a href="{{ route('livewire.show', ['categoria_slug' => $serieslug->slug, 'projeto_slug' => $next->slug ]) }}" class="link-next" id="link-next"><div class="seta_baixo"><img src="{{ asset('assets/img/layout/seta-scrolldown.svg')}}" alt=""></div></a>
            @endif


        </div>
        @endif

        @if($composicao->layout == 1)
        <div class="external_flex1">
            <div class="mobile_demo">
                @foreach($imagens as $img)
                    <img src="{{ asset('assets/img/projetos/imagens/'.$img->imagem)}}" alt="">
                @endforeach
            </div>
            <div class="pshow-imagembox1">
                @foreach($imagens as $img)
                <a href="{{ asset('assets/img/projetos/imagens/highres/'.$img->imagem) }}" data-lightbox="image-2" data-title="{{ str_replace(array("<p>", "</p>", "<strong>", "</strong>", "<bold>", "</bold>",),'', $img->description) }}}" rel="projeto"><div>
                    <img src="{{ asset('assets/img/projetos/imagens/'.$img->imagem)}}" alt="" class="img-projeto">
                </div></a>
                @endforeach
            </div>

            @if($next == 'nulo')
                <a style="display:none" href="" class="link-next"><div class="seta_baixo"><img src="{{ asset('assets/img/layout/seta-scrolldown.svg')}}" alt=""></div></a>
            @else
                <a href="{{ route('livewire.show', ['categoria_slug' => $serieslug->slug, 'projeto_slug' => $next->slug ]) }}" class="link-next" id="link-next2"><div class="seta_baixo"><img src="{{ asset('assets/img/layout/seta-scrolldown.svg')}}" alt=""></div></a>
            @endif

        </div>
        @endif
        
        @if($composicao->layout == 2)
        <div class="external_flex2">
            <div class="mobile_demo">
                @foreach($imagens as $img)
                    @if($img->ordem == '1')
                        <img class="imgs1" src="{{ asset('assets/img/projetos/imagens/'.$img->imagem)}}" alt="">
                    @endif
                @endforeach
                <div class="double">
                    @foreach($imagens as $img)
                        @if($img->ordem > '1')
                            <img class="imgs" src="{{ asset('assets/img/projetos/imagens/'.$img->imagem)}}" alt="">
                        @endif
                    @endforeach
                </div>
            </div>

            <div class="mobile_demo2">
                @foreach($imagens as $img)
                    @if($img->ordem == '1')
                        <a class="a_demo2" href="{{ asset('assets/img/projetos/imagens/highres/'.$img->imagem) }}" data-lightbox="image-3" data-title="{{ str_replace(array("<p>", "</p>", "<strong>", "</strong>", "<bold>", "</bold>",),'', $img->description) }}" rel="projeto">
                            <img class="imgs1" src="{{ asset('assets/img/projetos/imagens/'.$img->imagem)}}" alt="">
                        </a>
                    @endif
                @endforeach
                <div class="double">
                    @foreach($imagens as $img)
                        @if($img->ordem > '1')
                        <a href="{{ asset('assets/img/projetos/imagens/highres/'.$img->imagem) }}" data-lightbox="image-3" data-title="{{ str_replace(array("<p>", "</p>", "<strong>", "</strong>", "<bold>", "</bold>",),'', $img->description) }}" rel="projeto">
                            <img class="imgs" src="{{ asset('assets/img/projetos/imagens/'.$img->imagem)}}" alt="">
                        </a>
                        @endif
                    @endforeach
                </div>
            </div>
            <div class="pshow-imagembox">
                @foreach($imagens as $img)
                <a href="{{ asset('assets/img/projetos/imagens/highres/'.$img->imagem) }}" data-lightbox="image-3" data-title="{{ str_replace(array("<p>", "</p>", "<strong>", "</strong>", "<bold>", "</bold>",),'', $img->description) }}" rel="projeto"><div>
                    <img src="{{ asset('assets/img/projetos/imagens/'.$img->imagem)}}" alt="" class="img-projeto">
                </div></a>
                @endforeach
            </div>

            @if($next == 'nulo')
                <a style="display:none" href="" class="link-next"><div class="seta_baixo"><img src="{{ asset('assets/img/layout/seta-scrolldown.svg')}}" alt=""></div></a>
            @else
                <a href="{{ route('livewire.show', ['categoria_slug' => $serieslug->slug, 'projeto_slug' => $next->slug ]) }}" class="link-next" id="link-next3"><div class="seta_baixo"><img src="{{ asset('assets/img/layout/seta-scrolldown.svg')}}" alt=""></div></a>
            @endif 
            
        </div>
        @endif

        @if($composicao->layout == 3)
        <div class="external_flex3">
            <div class="mobile_demo">
                @foreach($imagens as $img)
                    <img src="{{ asset('assets/img/projetos/imagens/'.$img->imagem)}}" alt="">
                @endforeach
            </div>
            <div class="pshow-imagembox">
                @foreach($imagens as $img)
                <a href="{{ asset('assets/img/projetos/imagens/highres/'.$img->imagem) }}" data-lightbox="image-4" data-title="{{ str_replace(array("<p>", "</p>", "<strong>", "</strong>", "<bold>", "</bold>",),'', $img->description) }}" rel="projeto"><div>
                    <img src="{{ asset('assets/img/projetos/imagens/'.$img->imagem)}}" alt="" class="img-projeto">
                </div></a>
                @endforeach
            </div>

            @if($next == 'nulo')
                <a style="display:none" href="" class="link-next"><div class="seta_baixo"><img src="{{ asset('assets/img/layout/seta-scrolldown.svg')}}" alt=""></div></a>
            @else
                <a href="{{ route('livewire.show', ['categoria_slug' => $serieslug->slug, 'projeto_slug' => $next->slug ]) }}" class="link-next" id="link-next4"><div class="seta_baixo"><img src="{{ asset('assets/img/layout/seta-scrolldown.svg')}}" alt=""></div></a>
            @endif

        </div>
        @endif

        @if($composicao->layout == 4)
        <div class="external_flex4">
            <div class="flex4">
                <div class="texto_pt">
                    {!!$composicao->texto_pt!!}
                </div>
                <div class="texto_en">
                    {!!$composicao->texto_en!!}
                </div>
            </div>

            @if($next == 'nulo')
                <a style="display:none" href="" class="link-next"><div class="seta_baixo"><img src="{{ asset('assets/img/layout/seta-scrolldown.svg')}}" alt=""></div></a>
            @else
                <a href="{{ route('livewire.show', ['categoria_slug' => $serieslug->slug, 'projeto_slug' => $next->slug ]) }}" class="link-next" id="link-next4"><div class="seta_baixo"><img src="{{ asset('assets/img/layout/seta-scrolldown.svg')}}" alt=""></div></a>
            @endif
        </div>
        @endif

    </div>
</main>

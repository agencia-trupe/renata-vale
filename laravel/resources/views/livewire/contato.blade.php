<main id="swup" class="contato transition-fade">
    <div class="card1">

        @php
            $telefone = str_replace(array(" ", "-"), '', $contato->telefone);
        @endphp
        
        <div class="texto">
            <a href="https://api.whatsapp.com/send?phone=55{{$telefone}}&text=Ol%C3%A1%20tudo%20bem%3F" target="_blank"><p>+55 {{$contato->telefone}}</p></a>
        </div>

        <form action="{{ route('contato.post') }}" method="POST" class="form-contato" enctype="multipart/form-data">
            {!! csrf_field() !!}
            
            <div class="frontin">
                <label for="c_nome">nome</label>
                <input type="text" name="nome" value="" class="inputs" id="c_nome" required>
            </div>
            <div class="frontin">
                <label for="c_email">e-mail</label>
                <input type="email" name="email" value="" class="inputs" id="c_email" required>
            </div>
            <div class="frontin">
                <label for="c_telefone">telefone</label>
                <input type="text" name="telefone" class="inputs" id="c_telefone" value="">
            </div>
            
                <label for="c_msg">mensagem</label>
                <textarea name="mensagem" class="inputs_text" id="c_msg" required></textarea>
            
            <button type="submit" class="btn-enviar">[ ENVIAR » ]</button>
            
        </form>

        @if($errors->any())
        <div class="flash flash-erro">
            @foreach($errors->all() as $error)
            {!! $error !!}<br>
            @endforeach
        </div>
        @endif

        @if(session('enviado'))
        <div class="flash flash-sucesso">
            <p>Mensagem enviada com Sucesso</p>
        </div>
        @endif
    </div>

    <div class="card_image">
        <img src="{{ asset('assets/img/contato/'.$contato->imagem)}}">
    </div>
</main>

<main id="swup" class="politica-de-privacidade transition-fade">
    <div class="card1">
        
        <div class="pol">
            <h2 class="titulo">Política de Privacidade</h2>
            <div class="texto">{!! $politica->texto_pt !!}</div>
        </div>

    </div>

    <div class="card_image">
        <img src="{{ asset('assets/img/contato/'.$contato->imagem)}}">
    </div>
</main>

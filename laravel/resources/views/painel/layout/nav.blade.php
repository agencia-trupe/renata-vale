<ul class="nav navbar-nav">


    <li class="nav-item dropdown">
        <a href="#" class="nav-link dropdown-toggle px-3 @if(Tools::routeIs(['banners*'])) active @elseif(Tools::routeIs('parallax*')) active @endif d-flex align-items-center" role="button" id="navbarDarkDropdownMenuContatos" data-bs-toggle="dropdown" aria-expanded="false">
            Banners
            <i class="bi bi-caret-down-fill ms-1"></i>
        </a>
        <ul class="dropdown-menu dropdown-menu-dark" style="min-width: 160px" aria-labelledby="navbarDarkDropdownMenuBanners">
            <li>
                <a href="{{ route('banners.index') }}" class="nav-link px-3 @if(Tools::routeIs('banners*')) active @endif">Painel Principal</a>
            </li>
        </ul>
    </li>

    <li>
        <a href="{{ route('bio.index') }}" class="nav-link px-3 @if(Tools::routeIs('bio*')) active @endif">Bio</a>
    </li>

    <li>
        <a href="{{ route('projetos.index') }}" class="nav-link px-3 @if(Tools::routeIs('projetos*')) active @endif">Séries</a>
    </li>

    <li class="nav-item dropdown">
        <a href="#" class="nav-link dropdown-toggle px-3 @if(Tools::routeIs(['contatos*'])) active @endif d-flex align-items-center" role="button" id="navbarDarkDropdownMenuContatos" data-bs-toggle="dropdown" aria-expanded="false">
            Contatos
            @if($contatosNaoLidos >= 1)
            <span class="label label-success ms-1">{{ $contatosNaoLidos }}</span>
            @endif
            <i class="bi bi-caret-down-fill ms-1"></i>
        </a>
        <ul class="dropdown-menu dropdown-menu-dark" aria-labelledby="navbarDarkDropdownMenuContatos">
            <li>
                <a href="{{ route('contatos.index') }}" class="dropdown-item @if(Tools::routeIs('contatos.index')) active @endif">Informações de contato</a>
            </li>
            <li>
                <a href="{{ route('contatos-recebidos.index') }}" class="dropdown-item @if(Tools::routeIs('contatos-recebidos*')) active @endif d-flex align-items-center">
                    Contatos recebidos
                    @if($contatosNaoLidos >= 1)
                    <span class="label label-success ms-1">{{ $contatosNaoLidos }}</span>
                    @endif
                </a>
            </li>
        </ul>
    </li>

</ul>
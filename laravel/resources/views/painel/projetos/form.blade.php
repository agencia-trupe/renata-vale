@include('painel.layout.flash')

<div class="form-group">
    {!! Form::label('projetos_categoria_id', 'Série') !!}
    {!! Form::select('projetos_categoria_id', $categorias, old('projetos_categoria_id'), ['class' => 'form-control']) !!}
</div>


{{-- LAYOUT INICIO --}}
@if($submitText == 'Alterar')
@if($projeto->layout == 4)
<p class="mt-3"><strong>Selecione o Layout:</strong></p>
<div class="form-group mt-1 d-flex" style="align-items: center">
    <div class="mb-3 col-2">
        <input type="radio" id="horizon2" name="layout" value="0">
        <label for="horizon2">
            <img src="{{ url('assets/img/layout/horizontal-2.jpg') }}" style="height: 80px">
        </label><br>
    </div>
    <div class="mb-3 col-2">
        <input type="radio" id="horizon3" name="layout" value="1">
        <label for="horizon3">
            <img src="{{ url('assets/img/layout/horizontal-3.jpg') }}" style="height: 80px;margin-left: 10px">
        </label><br>
    </div>
    <div class="mb-3 col-2">
        <input type="radio" id="fit3" name="layout" value="2">
        <label for="fit3">
            <img src="{{ url('assets/img/layout/fit-3.jpg') }}" style="height: 80px">
        </label><br>
    </div>
    <div class="mb-3 col-2">
        <input type="radio" id="horizon7" name="layout" value="3">
        <label for="horizon7">
            <img src="{{ url('assets/img/layout/horizontal-7.jpg') }}" style="height: 80px;margin-left: 10px">
        </label><br>
    </div>
    <div class="mb-3 col-2">
        <input type="radio" id="horizon8" name="layout" value="4" checked>
        <label for="horizon8">
            <img src="{{ url('assets/img/layout/text.png') }}" style="height: 40px;margin-left: 10px">
        </label><br>
    </div>
</div>
@elseif($projeto->layout == 3)
<p class="mt-3"><strong>Selecione o Layout:</strong></p>
<div class="form-group mt-1 d-flex" style="align-items: center">
    <div class="mb-3 col-2">
        <input type="radio" id="horizon2" name="layout" value="0">
        <label for="horizon2">
            <img src="{{ url('assets/img/layout/horizontal-2.jpg') }}" style="height: 80px">
        </label><br>
    </div>
    <div class="mb-3 col-2">
        <input type="radio" id="horizon3" name="layout" value="1">
        <label for="horizon3">
            <img src="{{ url('assets/img/layout/horizontal-3.jpg') }}" style="height: 80px;margin-left: 10px">
        </label><br>
    </div>
    <div class="mb-3 col-2">
        <input type="radio" id="fit3" name="layout" value="2">
        <label for="fit3">
            <img src="{{ url('assets/img/layout/fit-3.jpg') }}" style="height: 80px">
        </label><br>
    </div>
    <div class="mb-3 col-2">
        <input type="radio" id="horizon7" name="layout" value="3" checked>
        <label for="horizon7">
            <img src="{{ url('assets/img/layout/horizontal-7.jpg') }}" style="height: 80px;margin-left: 10px">
        </label><br>
    </div>
    <div class="mb-3 col-2">
        <input type="radio" id="horizon8" name="layout" value="4">
        <label for="horizon8">
            <img src="{{ url('assets/img/layout/text.png') }}" style="height: 40px;margin-left: 10px">
        </label><br>
    </div>
</div>
@elseif($projeto->layout == 2)
<p class="mt-3"><strong>Selecione o Layout:</strong></p>
<div class="form-group mt-1 d-flex" style="align-items: center">
    <div class="mb-3 col-2">
        <input type="radio" id="horizon2" name="layout" value="0">
        <label for="horizon2">
            <img src="{{ url('assets/img/layout/horizontal-2.jpg') }}" style="height: 80px">
        </label><br>
    </div>
    <div class="mb-3 col-2">
        <input type="radio" id="horizon3" name="layout" value="1">
        <label for="horizon3">
            <img src="{{ url('assets/img/layout/horizontal-3.jpg') }}" style="height: 80px;margin-left: 10px">
        </label><br>
    </div>
    <div class="mb-3 col-2">
        <input type="radio" id="fit3" name="layout" value="2" checked>
        <label for="fit3">
            <img src="{{ url('assets/img/layout/fit-3.jpg') }}" style="height: 80px">
        </label><br>
    </div>
    <div class="mb-3 col-2">
        <input type="radio" id="horizon7" name="layout" value="3">
        <label for="horizon7">
            <img src="{{ url('assets/img/layout/horizontal-7.jpg') }}" style="height: 80px;margin-left: 10px">
        </label><br>
    </div>
    <div class="mb-3 col-2">
        <input type="radio" id="horizon8" name="layout" value="4">
        <label for="horizon8">
            <img src="{{ url('assets/img/layout/text.png') }}" style="height: 40px;margin-left: 10px">
        </label><br>
    </div>
</div>
@elseif($projeto->layout == 1)
<p class="mt-3"><strong>Selecione o Layout:</strong></p>
<div class="form-group mt-1 d-flex" style="align-items: center">
    <div class="mb-3 col-2">
        <input type="radio" id="horizon2" name="layout" value="0">
        <label for="horizon2">
            <img src="{{ url('assets/img/layout/horizontal-2.jpg') }}" style="height: 80px">
        </label><br>
    </div>
    <div class="mb-3 col-2">
        <input type="radio" id="horizon3" name="layout" value="1" checked>
        <label for="horizon3">
            <img src="{{ url('assets/img/layout/horizontal-3.jpg') }}" style="height: 80px;margin-left: 10px">
        </label><br>
    </div>
    <div class="mb-3 col-2">
        <input type="radio" id="fit3" name="layout" value="2">
        <label for="fit3">
            <img src="{{ url('assets/img/layout/fit-3.jpg') }}" style="height: 80px">
        </label><br>
    </div>
    <div class="mb-3 col-2">
        <input type="radio" id="horizon7" name="layout" value="3">
        <label for="horizon7">
            <img src="{{ url('assets/img/layout/horizontal-7.jpg') }}" style="height: 80px;margin-left: 10px">
        </label><br>
    </div>
    <div class="mb-3 col-2">
        <input type="radio" id="horizon8" name="layout" value="4">
        <label for="horizon8">
            <img src="{{ url('assets/img/layout/text.png') }}" style="height: 40px;margin-left: 10px">
        </label><br>
    </div>
</div>
@elseif($projeto->layout == 0)
<p class="mt-3"><strong>Selecione o Layout:</strong></p>
<div class="form-group mt-1 d-flex" style="align-items: center">
    <div class="mb-3 col-2">
        <input type="radio" id="horizon2" name="layout" value="0" checked>
        <label for="horizon2">
            <img src="{{ url('assets/img/layout/horizontal-2.jpg') }}" style="height: 80px">
        </label><br>
    </div>
    <div class="mb-3 col-2">
        <input type="radio" id="horizon3" name="layout" value="1">
        <label for="horizon3">
            <img src="{{ url('assets/img/layout/horizontal-3.jpg') }}" style="height: 80px;margin-left: 10px">
        </label><br>
    </div>
    <div class="mb-3 col-2">
        <input type="radio" id="fit3" name="layout" value="2">
        <label for="fit3">
            <img src="{{ url('assets/img/layout/fit-3.jpg') }}" style="height: 80px">
        </label><br>
    </div>
    <div class="mb-3 col-2">
        <input type="radio" id="horizon7" name="layout" value="3">
        <label for="horizon7">
            <img src="{{ url('assets/img/layout/horizontal-7.jpg') }}" style="height: 80px;margin-left: 10px">
        </label><br>
    </div>
    <div class="mb-3 col-2">
        <input type="radio" id="horizon8" name="layout" value="4">
        <label for="horizon8">
            <img src="{{ url('assets/img/layout/text.png') }}" style="height: 40px;margin-left: 10px">
        </label><br>
    </div>
</div>
@endif
{{-- Projeto NOVO --}}
@else
<p class="mt-3"><strong>Selecione o Layout:</strong></p>
<div class="form-group mt-1 d-flex" style="align-items: center">
    <div class="mb-3 col-2">
        <input type="radio" id="horizon2" name="layout" value="0">
        <label for="horizon2">
            <img src="{{ url('assets/img/layout/horizontal-2.jpg') }}" style="height: 80px">
        </label><br>
    </div>
    <div class="mb-3 col-2">
        <input type="radio" id="horizon3" name="layout" value="1">
        <label for="horizon3">
            <img src="{{ url('assets/img/layout/horizontal-3.jpg') }}" style="height: 80px;margin-left: 10px">
        </label><br>
    </div>
    <div class="mb-3 col-2">
        <input type="radio" id="fit3" name="layout" value="2">
        <label for="fit3">
            <img src="{{ url('assets/img/layout/fit-3.jpg') }}" style="height: 80px">
        </label><br>
    </div>
    <div class="mb-3 col-2">
        <input type="radio" id="horizon7" name="layout" value="3">
        <label for="horizon7">
            <img src="{{ url('assets/img/layout/horizontal-7.jpg') }}" style="height: 80px;margin-left: 10px">
        </label><br>
    </div>
    <div class="mb-3 col-2">
        <input type="radio" id="horizon8" name="layout" value="4">
        <label for="horizon8">
            <img src="{{ url('assets/img/layout/text.png') }}" style="height: 40px;margin-left: 10px">
        </label><br>
    </div>
</div>
@endif
{{-- LAYOUT FIM --}}

<div class="form-group mt-3">
    {!! Form::label('titulo', 'Título') !!}
    {!! Form::text('titulo', null, ['class' => 'form-control']) !!}
</div>


{{-- Bloco de texto VALIDADO se for Editado --}}
@if($submitText == 'Alterar')
@if($projeto->layout == 4)
<div class="col-12 mt-5" id="texts" style="display:block">
    <h4 class="mb-3">Texto da Série:</h4>
    <div class="d-flex" style="column-gap: 20px; width: 1300px;">
        <div class="mb-3" style="width: 50%">
            {!! Form::label('texto_pt', 'Texto PT', ['class' => 'mt-2 mb-3']) !!}
            {!! Form::textarea('texto_pt', null, ['class' => 'form-control editor-padrao']) !!}
        </div>
        <div class="mb-3" style="width: 50%">
            {!! Form::label('texto_en', 'Texto EN', ['class' => 'mt-2 mb-3']) !!}
            {!! Form::textarea('texto_en', null, ['class' => 'form-control editor-padrao']) !!}
        </div>
    </div>
</div>
@else
<div class="col-12 mt-5" id="texts" style="display:none">
    <h4 class="mb-3">Texto da Série:</h4>
    <div class="d-flex" style="column-gap: 20px; width: 1300px;">
        <div class="mb-3" style="width: 50%">
            {!! Form::label('texto_pt', 'Texto PT', ['class' => 'mt-2 mb-3']) !!}
            {!! Form::textarea('texto_pt', null, ['class' => 'form-control editor-padrao']) !!}
        </div>
        <div class="mb-3" style="width: 50%">
            {!! Form::label('texto_en', 'Texto EN', ['class' => 'mt-2 mb-3']) !!}
            {!! Form::textarea('texto_en', null, ['class' => 'form-control editor-padrao']) !!}
        </div>
    </div>
</div>
@endif
@else
<div class="col-12 mt-5" id="texts" style="display:none">
    <h4 class="mb-3">Texto da Série:</h4>
    <div class="d-flex" style="column-gap: 20px; width: 1300px;">
        <div class="mb-3" style="width: 50%">
            {!! Form::label('texto_pt', 'Texto PT', ['class' => 'mt-2 mb-3']) !!}
            {!! Form::textarea('texto_pt', null, ['class' => 'form-control editor-padrao']) !!}
        </div>
        <div class="mb-3" style="width: 50%">
            {!! Form::label('texto_en', 'Texto EN', ['class' => 'mt-2 mb-3']) !!}
            {!! Form::textarea('texto_en', null, ['class' => 'form-control editor-padrao']) !!}
        </div>
    </div>
</div>
@endif
{{-- FIM da validação do bloco de texto --}}

{!! Form::submit($submitText, ['class' => 'btn btn-success mt-3']) !!}

<a href="{{ route('projetos.index') }}" class="btn btn-default btn-light mt-3">Voltar</a>

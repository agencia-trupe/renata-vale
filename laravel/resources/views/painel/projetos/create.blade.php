@extends('painel.layout.template')

@section('content')

    <legend>
        <h2><small>Séries /</small> Adicionar Composição</h2>
    </legend>

    {!! Form::open(['route' => 'projetos.store', 'files' => true]) !!}

        @include('painel.projetos.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection

@extends('painel.layout.template')

@section('content')

    <legend>
        <h2><small>Projetos / {{ $projeto->titulo }} /</small> Editar Vídeo</h2>
    </legend>

    {!! Form::model($video, [
        'route'  => ['projetos.videos.update', $projeto->id, $video->id],
        'method' => 'patch'])
    !!}

    @include('painel.projetos.videos.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection

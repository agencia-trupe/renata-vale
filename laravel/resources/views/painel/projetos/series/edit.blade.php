@extends('painel.layout.template')

@section('content')

    <legend>
        <h2><small>Projetos /</small> Editar Série</h2>
    </legend>

    {!! Form::model($series, [
        'route'  => ['series.update', $series->id],
        'method' => 'patch'])
    !!}

    @include('painel.projetos.series.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection

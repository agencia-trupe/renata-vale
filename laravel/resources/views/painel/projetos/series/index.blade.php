@extends('painel.layout.template')

@section('content')

    @include('painel.layout.flash')

    <a href="{{ route('projetos.index') }}" title="Voltar para Projetos" class="btn btn-sm btn-primary mt-3 mb-3 col-2">
        &larr; Voltar para Composições    </a>

    <legend>
        <h2>
            <small>Gerenciar /</small> Séries

            <a href="{{ route('series.create') }}" class="btn btn-success col-2 mt-3"><span class="glyphicon glyphicon-plus" style="margin-right:10px;"></span>Adicionar Série</a>
        </h2>
    </legend>

    @if(!count($series))
    <div class="alert alert-warning" role="alert">Nenhuma serie cadastrada.</div>
    @else
    <table class="table table-striped table-bordered table-hover table-sortable" data-table="projetos_categorias">
        <thead>
            <tr>
                <th>Ordenar</th>
                <th>Título</th>
                <th><span class="glyphicon glyphicon-cog"></span></th>
            </tr>
        </thead>

        <tbody>
        @foreach ($series as $serie)
            <tr class="tr-row" id="{{ $serie->id }}">
                <td>
                    <a href="#" class="btn btn-dark btn-sm btn-move">
                        <i class="bi bi-arrows-move"></i>
                    </a>
                </td>
                <td>{{ $serie->titulo }}</td>
                <td class="crud-actions">
                    
                    {!! Form::open([
                    'route' => ['series.destroy', $serie->id],
                    'method' => 'delete'
                    ]) !!}

                    <div class="btn-group btn-group-sm">
                        <a href="{{ route('series.edit', $serie->id ) }}" class="btn btn-primary btn-sm pull-left">
                            <span class="glyphicon glyphicon-pencil" style="margin-right:10px;"></span>Editar
                        </a>

                        <button type="submit" class="btn btn-danger btn-sm btn-delete"><span class="glyphicon glyphicon-remove" style="margin-right:10px;"></span>Excluir</button>
                    </div>

                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    @endif

@endsection

@include('painel.layout.flash')

<div class="form-group">
    {!! Form::label('titulo', 'Título') !!}
    {!! Form::text('titulo', null, ['class' => 'form-control']) !!}
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success col-1 mt-3 mb-3']) !!}

<a href="{{ route('series.index') }}" class="btn btn-sm btn-primary col-1">Voltar</a>

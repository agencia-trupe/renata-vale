@extends('painel.layout.template')

@section('content')

    <legend>
        <h2><small>Projetos /</small> Adicionar Série</h2>
    </legend>

    {!! Form::open(['route' => 'series.store']) !!}

        @include('painel.projetos.series.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection

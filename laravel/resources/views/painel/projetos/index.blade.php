@extends('painel.layout.template')

@section('content')

@include('painel.layout.flash')

<legend class="d-flex flex-row align-items-center justify-content-between mb-4">
    <h2 class="m-0">Composição</h2>
    <div class="d-flex flex-row align-items-center">
        <a href="{{ route('series.index') }}" class="btn btn-primary btn-sm pull-right"><span class="glyphicon glyphicon-edit" style="margin-right:10px;"></span>Gerenciar Séries</a>
        <a href="{{ route('projetos.create') }}" class="btn btn-success btn-sm pull-right"><span class="glyphicon glyphicon-plus" style="margin-right:10px;"></span>Criar Composição</a>
    </div>
</legend>

<div class="mt-3 mb-5">
    <h4>Séries:</h4>
    <div class="row ps-2">
        @foreach($categorias as $categoria)
            {{-- <a href="{{ route('projetos.index', ['categoria' => $categoria->id]) }}" class="btn btn-secondary col-12 col-md-2 m-1 {{ (isset($_GET['categoria']) && $_GET['categoria'] == $categoria->id) ? 'active' : '' }}">{{ $categoria->titulo }}</a> --}}
            <a href="{{route('grupo-cat', ['group' => $categoria->slug])}}" @if($categoria->id == '0') class="btn btn-primary col-12 col-md-2 m-1" @else class="btn btn-secondary col-12 col-md-2 m-1" @endif>{{ $categoria->titulo }}</a>
        @endforeach
    </div>
</div>

@if(!count($registros))
<div class="alert alert-warning" role="alert">Nenhum registro encontrado.</div>
@else
<table class="table table-striped table-bordered table-hover table-sortable" data-table="projetos">
    <thead>
        <tr>
            <th>Ordenar</th>
            <th>Série</th>
            <th>Título</th>
            <th>Imagens</th>
            <th>Gerenciar</th>
        </tr>
    </thead>

    <tbody>
        @foreach ($registros as $registro)
        <tr class="tr-row" id="{{ $registro->id }}">
            
            <td>
                <a href="#" class="btn btn-dark btn-sm btn-move">
                    <i class="bi bi-arrows-move"></i>
                </a>
            </td>

            <td>
                @if($registro->categoria)
                {{ $registro->categoria->slug }}
                @else
                <span class="label label-warning">sem categoria</span>
                @endif
            </td>

            <td>{{ $registro->titulo }}</td>

            @if($registro->layout == 4)
                <td><strong>Página de Texto</strong></td>
            @else
                <td><a href="{{ route('projetos.imagens.index', $registro->id) }}" class="btn btn-secondary btn-gerenciar btn-sm">
                    <span class="glyphicon glyphicon-picture" style="margin-right:10px;"></span>Gerenciar
                </a></td>
            @endif
            
            <td class="crud-actions">
                {!! Form::open([
                'route' => ['projetos.destroy', $registro->id],
                'method' => 'delete'
                ]) !!}

                <div class="btn-group btn-group-sm">
                    <a href="{{ route('projetos.edit', $registro->id ) }}" class="btn btn-primary btn-sm pull-left">
                        <span class="glyphicon glyphicon-pencil" style="margin-right:10px;"></span>Editar
                    </a>

                    <button type="submit" class="btn btn-danger btn-sm btn-delete"><span class="glyphicon glyphicon-remove" style="margin-right:10px;"></span>Excluir</button>
                </div>

                {!! Form::close() !!}
            </td>

        </tr>
        @endforeach
    </tbody>
</table>
@endif

@endsection
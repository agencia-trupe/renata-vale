@extends('painel.layout.template')

@section('content')

    <legend>
        <h2>Adicionar Imagem</h2>
    </legend>
    
    {!! Form::open([
        'route' => ['projetos.imagens.store', $projeto->id] , 
        'files' => true]) !!}

        @include('painel.projetos.imagens.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection

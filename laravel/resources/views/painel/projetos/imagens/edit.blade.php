@extends('painel.layout.template')

@section('content')

    <legend>
        <h2>Editar Imagem</h2>
    </legend>

    {!! Form::model($imagen, [
        'route'  => ['projetos.imagens.update', ['projeto' => $projeto->id, 'imagen' => $imagen->id]],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.projetos.imagens.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection

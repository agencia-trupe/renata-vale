@extends('painel.layout.template')

@section('content')

@include('painel.layout.flash')

<legend class="mb-4">
    <h2 class="mb-4">Imagens</h2>
    <div class="d-flex flex-row align-items-center">
        <a href="{{ route('projetos.imagens.create', ['projeto' => $projeto->id]) }}" class="btn btn-success pull-right mb-2" style="width: 30%; margin-left:auto; margin-right:auto;"><span class="glyphicon glyphicon-plus" style="margin-right:10px;"></span>Subir Imagem</a>
    </div>
</legend>


@if(!count($registros))
<div class="alert alert-warning" role="alert">Nenhuma imagem encontrada.</div>
@else
<table class="table table-striped table-bordered table-hover table-sortable" data-table="projetos_imagens">
    <thead>
        <tr>
            <th>Ordenar</th>
            <th>Imagem</th>
            <th>Descritivo</th>
            <th>Gerenciar</th>
        </tr>
    </thead>

    <tbody>
        @foreach ($registros as $registro)
        <tr class="tr-row" id="{{ $registro->id }}">
          
            <td>
                <a href="#" class="btn btn-dark btn-sm btn-move" style="display:flex; max-width:80px; margin-left:auto; margin-right:auto">
                    <i class="bi bi-arrows-move"></i>
                </a>
            </td>
            
            <td><img src="{{ asset('assets/img/projetos/imagens/'.$registro->imagem) }}" style="width: 100%; display:block; max-width:80px; margin-left:auto; margin-right:auto" alt=""></td>
            
            <td>{!! $registro->description !!}</td>

            <td class="crud-actions">
                {!! Form::open([
                'route' => ['projetos.imagens.destroy', ['projeto' => $projeto->id, 'imagen' => $registro->id]],
                'method' => 'delete'
                ]) !!}

                <div class="btn-group btn-group-sm">
                    <a href="{{ route('projetos.imagens.edit', ['projeto' => $projeto->id, 'imagen' => $registro->id] ) }}" class="btn btn-primary btn-sm pull-left">
                        <span class="glyphicon glyphicon-pencil" style="margin-right:10px;"></span>Editar
                    </a>

                    <button type="submit" class="btn btn-danger btn-sm btn-delete"><span class="glyphicon glyphicon-remove" style="margin-right:10px;"></span>Excluir</button>
                </div>

                {!! Form::close() !!}
            </td>
        </tr>
        @endforeach

    </tbody>
</table>
@endif
<a href="{{ route('projetos.index') }}" title="Voltar para Projetos" class="btn btn-secondary mt-4 mb-3 col-2">
    &larr; Voltar para Composições</a>

@endsection
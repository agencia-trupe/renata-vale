@include('painel.layout.flash')

<input type="hidden" name="projeto_id" value="{{$projeto->id}}">

<div class="mb-3 col-12 col-md-12">
    {!! Form::label('imagem', 'Imagem') !!}
    @if($submitText == 'Alterar')
    @if($imagen->imagem)
    <img src="{{ url('assets/img/projetos/imagens/'.$imagen->imagem) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
    @endif
    @endif
    {!! Form::file('imagem', ['class' => 'form-control']) !!}
</div>

<div class="mb-3 mt-3 col-12">
    {!! Form::label('description', 'Descritivo') !!}
    {!! Form::textarea('description', null, ['class' => 'form-control editor-padrao']) !!}
</div>


{!! Form::submit($submitText, ['class' => 'btn btn-success mt-3']) !!}

<a href="{{ route('projetos.imagens.index', $projeto->id) }}" class="btn btn-default btn-light mt-3">Voltar</a>

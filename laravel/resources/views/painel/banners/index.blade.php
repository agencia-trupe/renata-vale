@extends('painel.layout.template')

@section('content')

@include('painel.layout.flash')

<legend class="d-flex flex-row align-items-center justify-content-between mb-4">
    <h2 class="m-0">BANNERS</h2>

    <a href="{{ route('banners.create') }}" class="btn btn-success btn-sm">
        <i class="bi bi-plus-circle me-2 mb-1"></i>
        Adicionar Banner
    </a>
</legend>


@if(!count($banners))
<div class="alert alert-warning" role="alert">Nenhum registro encontrado.</div>
@else
<div class="table-responsive">
    <table class="table table-striped table-bordered table-hover table-sortable" data-table="banners">
        <thead>
            <tr>
                <th scope="col">Ordenar</th>
                <th scope="col">Imagem</th>
                <th scope="col">Frase</th>
                <th class="no-filter" scope="col"><i class="bi bi-gear-fill me-2"></th>
            </tr>
        </thead>

        <tbody>
            @foreach ($banners as $banner)
            <tr id="{{ $banner->id }}">
                <td>
                    <a href="#" class="btn btn-dark btn-sm btn-move">
                        <i class="bi bi-arrows-move"></i>
                    </a>
                </td>
                <td>
                    <img src="{{ asset('assets/img/banners/'.$banner->imagem) }}" style="width: auto; max-width:100px;" alt="">
                </td>
                <td>
                    <p>{{$banner->frase}}</p>
                </td>
                <td class="crud-actions">
                    {!! Form::open([
                    'route' => ['banners.destroy', $banner->id],
                    'method' => 'delete'
                    ]) !!}

                    <div class="btn-group btn-group-sm" role="group">
                        <a href="{{ route('banners.edit', $banner->id ) }}" class="btn btn-primary btn-sm">
                            <i class="bi bi-pencil-fill me-2"></i>Editar
                        </a>

                        <button type="submit" class="btn btn-danger btn-sm btn-delete"><i class="bi bi-trash-fill me-2"></i>Excluir</button>
                    </div>

                    {!! Form::close() !!}
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
@endif

@endsection
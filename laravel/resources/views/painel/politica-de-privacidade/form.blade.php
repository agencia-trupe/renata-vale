@include('painel.layout.flash')

<div class="mb-3 col-12">
    {!! Form::label('texto_pt', 'Texto') !!}
    {!! Form::textarea('texto_pt', null, ['class' => 'form-control editor-padrao']) !!}
</div>

<div class="d-flex align-items-center mt-4">
    {!! Form::submit($submitText, ['class' => 'btn btn-success me-1']) !!}

    <a href="{{ route('politica-de-privacidade.index') }}" class="btn btn-secondary btn-voltar">Voltar</a>
</div>
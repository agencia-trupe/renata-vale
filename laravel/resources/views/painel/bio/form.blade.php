@include('painel.layout.flash')


<div class="mb-3 col-12 col-md-12">
    {!! Form::label('imagem', 'Imagem') !!}
    @if($submitText == 'Alterar')
    @if($bio->imagem)
    <img src="{{ url('assets/img/bio/'.$bio->imagem) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
    @endif
    @endif
    {!! Form::file('imagem', ['class' => 'form-control']) !!}
</div>

<div class="mb-3 col-12">
    {!! Form::label('texto_1', 'Texto 1') !!}
    {!! Form::textarea('texto_1', null, ['class' => 'form-control editor-padrao']) !!}
</div>

<hr class="mt-5 mb-4">

<div class="mb-3 col-12">
    {!! Form::label('texto_2', 'Texto 2') !!}
    {!! Form::textarea('texto_2', null, ['class' => 'form-control editor-padrao']) !!}
</div>


<div class="d-flex align-items-center mt-4">
    {!! Form::submit($submitText, ['class' => 'btn btn-success me-1']) !!}

    <a href="{{ route('bio.index') }}" class="btn btn-secondary btn-voltar">Voltar</a>
</div>
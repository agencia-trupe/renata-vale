@extends('painel.layout.template')

@section('content')

<legend class="mb-4">
    <h2 class="m-0">POLÍTICA DE PRIVACIDADE</h2>
</legend>

{!! Form::model($bio, [
'route' => ['bio.update', $bio->id],
'method' => 'patch',
'files' => true])
!!}

@include('painel.bio.form', ['submitText' => 'Alterar'])

{!! Form::close() !!}

@endsection
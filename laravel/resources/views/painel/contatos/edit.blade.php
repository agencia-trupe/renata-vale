@extends('painel.layout.template')

@section('content')

<legend class="mb-4">
    <h2 class="m-0">CONTATOS - Informações de Contato</h2>
</legend>

{!! Form::model($contato, [
'route' => ['contatos.update', $contato->id],
'method' => 'patch',
'files' => true])
!!}

@include('painel.contatos.form', ['submitText' => 'Alterar'])

{!! Form::close() !!}

@endsection
@include('painel.layout.flash')

<div class="mb-3">
    {!! Form::label('name', 'Usuário', ['class' => 'form-label']) !!}
    {!! Form::text('name', null, ['class' => 'form-control input-text']) !!}
</div>

<div class="mb-3">
    {!! Form::label('email', 'E-mail', ['class' => 'form-label']) !!}
    @if($submitText == 'Alterar')
    {!! Form::email('email', null, ['class' => 'form-control input-text', 'disabled' => 'disabled']) !!}
    @else
    {!! Form::email('email', null, ['class' => 'form-control input-text']) !!}
    @endif
</div>

<div class="mb-3">
    {!! Form::label('password', 'Senha', ['class' => 'form-label']) !!}
    {!! Form::password('password', ['class' => 'form-control input-text']) !!}
</div>

<div class="mb-3">
    {!! Form::label('password_confirmation', 'Confirmação de Senha', ['class' => 'form-label']) !!}
    {!! Form::password('password_confirmation', ['class' => 'form-control input-text']) !!}
</div>

<div class="d-flex align-items-center mt-4">
    {!! Form::submit($submitText, ['class' => 'btn btn-success me-1']) !!}

    <a href="{{ route('usuarios.index') }}" class="btn btn-secondary btn-voltar">Voltar</a>
</div>
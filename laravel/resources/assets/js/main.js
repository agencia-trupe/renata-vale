import axios from 'axios';

import AjaxSetup from './AjaxSetup';
import MobileToggle from './MobileToggle';

AjaxSetup();
MobileToggle();


try{
  //Swup
  const swup = new Swup({
    plugins: [new SwupLivewirePlugin()]
  });
} 
catch{
  console.log("Pagina sem swup");
}




//  FANCYBOX MIDIAS GALERIAS

 $(".fancybox, .projeto-imagem").fancybox({
  padding: 0,
  prevEffect: "fade",
  nextEffect: "fade",
  closeBtn: false,
  openEffect: "elastic",
  openSpeed: "150",
  closeEffect: "elastic",
  closeSpeed: "150",
  keyboard: true,
  helpers: {
    title: {
      type: "outside",
      position: "top",
    },
    overlay: {
      css: {
        background: "rgba(132, 134, 136, .88)",
      },
    },
  },
  mobile: {
    clickOutside: "close",
  },
  fitToView: false,
  autoSize: false,
  beforeShow: function () {
    this.maxWidth = "100%";
    this.maxHeight = "100%";
  },
});


$(".projeto-imagem").click(function handler(e) {
  e.preventDefault();

  const id = $(this).data("galeria-id");

  if (id) {
    $(`a[rel=galeria-${id}]`)[0].click();
  }
});



  // AVISO DE COOKIES
$(document).ready(function() {
  
  $(".aviso-cookies").hide();

  if (window.location.href == routeHome) {
    $(".aviso-cookies").show();

    $(".aceitar-cookies").click(function () {
      var url = window.location.origin + window.location.pathname + "aceite-de-cookies";

      $.ajax({
        type: "POST",
        url: url,
        success: function (data, textStatus, jqXHR) {
          $(".aviso-cookies").hide();
        },
        error: function (jqXHR, textStatus, errorThrown) {
          console.log(jqXHR, textStatus, errorThrown);
        },
      });
    });
  }
});

import Clipboard from "./modules/Clipboard.js";
import DataTables from "./modules/DataTables.js";
import DatePicker from "./modules/DatePicker.js";
import DeleteButton from "./modules/DeleteButton.js";
import ImagesUpload from "./modules/ImagesUpload.js";
import OrderImages from "./modules/OrderImages.js";
import OrderTable from "./modules/OrderTable.js";
import TextEditor from "./modules/TextEditor.js";
import MobileToggle from "../js/MobileToggle";

Clipboard();
DataTables();
DatePicker();
DeleteButton();
ImagesUpload();
OrderImages();
OrderTable();
TextEditor();
MobileToggle();

$(document).ready(function () {
    
});


//SERIES EXIBIR TEXTO

try{
    var text = document.getElementById("horizon8");
    text.addEventListener("click", abrirDiv);

    var horizon3 = document.getElementById("horizon3");
    horizon3.addEventListener("click", fecharDiv);

    var horizon7 = document.getElementById("horizon7");
    horizon7.addEventListener("click", fecharDiv);

    var fit3 = document.getElementById("fit3");
    fit3.addEventListener("click", fecharDiv);

    var horizon2 = document.getElementById("horizon2");
    horizon2.addEventListener("click", fecharDiv);

    function abrirDiv() {
        var divs = document.getElementById("texts");
        if (divs.style.display === "none") {
            divs.style.display = "block";
        }
    }

    function fecharDiv() {
        var divs = document.getElementById("texts");
        if (divs.style.display === "block") {
            divs.style.display = "none";
        }
    }

}catch{}
